/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author root
 */
public class PoligonalFechada extends Poligonal {
    
    private Ponto2D [] vertices;
    
    public PoligonalFechada(Ponto2D [] vertices){                
        super(vertices);
        this.vertices = vertices;        
    }
    
    @Override
    public double getComprimento(){
        //Soma das distancias entre os vertices (poligonal fechada)
        double soma=0;        
        soma += this.vertices[0].dist(this.vertices[1]);
        soma += this.vertices[1].dist(this.vertices[2]);
        soma += this.vertices[0].dist(this.vertices[2]);
        
        return soma;
    }
}
