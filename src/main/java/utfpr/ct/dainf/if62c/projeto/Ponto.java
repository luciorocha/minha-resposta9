package utfpr.ct.dainf.if62c.projeto;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * @author
 */
public class Ponto {
    private double x, y, z;

    public Ponto(){
        this.x=0;
        this.y=0;
        this.z=0;
    }

    public Ponto(double x, double y, double z){
        this.x=x;
        this.y=y;
        this.z=z;
    }
    
    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }
    
    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setZ(double z) {
        this.z = z;
    }
    
    @Override
    public String toString(){        
        return String.format(
                "%s(%f,%f,%f)", getNome(), this.x,this.y,this.z);
    }
    
    @Override
    public boolean equals(Object p1){
        
        Ponto p = (Ponto) p1;
        if (this.x == p.getX() && this.y == p.getY() && this.z == p.getZ())
            return true;
        else
            return false;        
    }
    
    public double dist(Ponto p){
        return Math.sqrt(Math.pow(p.getX()-this.x, 2)+Math.pow(p.getY()-this.y, 2)+Math.pow(p.getZ()-this.z, 2));
    }
    
    /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    public String getNome() {
        return getClass().getSimpleName();
    }

}
