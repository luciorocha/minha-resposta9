/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author root
 * @param <T>
 */
public class Poligonal <T extends Ponto2D> {
    
    private T[] vertices;    
    
    public Poligonal(T vertices[]){
        this.vertices = vertices;
        
        if (vertices.length < 2)
            throw new RuntimeException("Poligonal deve ter ao menos 2 vértices");
    }
    
    public int getN(){
        return this.vertices.length;
    }
    
    public T get(int a){
        if (a<0 || a>this.vertices.length)
            return null;
        else
            return this.vertices[a];
    }
    
    public void set(int e, T b){
        this.vertices[e] = b;
    }
    
    public double getComprimento(){
        //Soma das distancias entre os vertices (diagonal aberta)
        double soma=0;
        soma += this.vertices[0].dist(this.vertices[1]);
        soma += this.vertices[1].dist(this.vertices[2]);
        
        return soma;
    }
    
}
