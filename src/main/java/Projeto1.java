import utfpr.ct.dainf.if62c.projeto.*;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author 
 */
public class Projeto1 {

    public static void main(String[] args) {
        PontoXZ p1 = new PontoXZ(-3,2);
        PontoXZ p2 = new PontoXZ(-3,6);
        PontoXZ p3 = new PontoXZ(0,2);
        Ponto2D [] vertices = { p1, p2, p3 };        
        PoligonalFechada p = new PoligonalFechada(vertices);
        System.out.println("Comprimento da poligonal = "+p.getComprimento());
    }
    
}
